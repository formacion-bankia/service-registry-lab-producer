package com.minsait.labs.api;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-09-08T10:04:57.715+02:00[Europe/Paris]")
public class NotFoundException extends ApiException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8998089240918151567L;
	/**
	 * 
	 */
	private int code;
    public NotFoundException (int code, String msg) {
        super(code, msg);
        this.code = code;
    }
}
