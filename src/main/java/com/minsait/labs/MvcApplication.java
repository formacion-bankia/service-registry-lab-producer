package com.minsait.labs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class MvcApplication{

    public static void main(String[] args) throws Exception {
        new SpringApplication(MvcApplication.class).run(args);
    }
}
